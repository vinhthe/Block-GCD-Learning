//
//  AppDelegate.h
//  Block And GCD Appcoda
//
//  Created by Vinh The on 6/5/16.
//  Copyright © 2016 Vinh The. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

